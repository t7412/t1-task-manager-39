package ru.t1.chubarov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.model.Session;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO tm_session (id, date, user_id, role) VALUES (#{id}, #{date}, #{userId}, #{role})")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    void add(@NotNull final Session session);

    @NotNull
    @Select("SELECT * FROM tm_session")
    List<Session> findAll() throws Exception;

    @NotNull
    @Select("SELECT * FROM tm_session WHERE user_id = #{userId}")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    List<Session> findAllByUser(@Nullable @Param("userId") String userId) throws Exception;

    @Nullable
    @Select("SELECT * FROM tm_session WHERE id = #{id} AND user_id =#{userId} LIMIT 1")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    Session findOneById(@Nullable @Param("userId") String userId, @NotNull @Param("id") String id) throws Exception;

    @Select("SELECT COUNT(*) FROM tm_session WHERE id = #{id} AND user_id = #{userId}")
    int existsById(@Nullable @Param("userId") String userId, @NotNull @Param("id") String id) throws Exception;

    @Delete("DELETE FROM tm_session WHERE id = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
            , @Result(property = "id", column = "id")
    })
    void remove(@NotNull final Session session) throws Exception;

    @Delete("DELETE FROM tm_session WHERE id = #{id} AND user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    void removeOneById(@Nullable @Param("userId") String userId, @Nullable @Param("id") String id) throws Exception;

    @Delete("DELETE FROM tm_session WHERE id = #{id}")
    void removeOneById(@Nullable @Param("id") String id) throws Exception;

    @Select("SELECT COUNT(*) FROM tm_session")
    int getSize() throws Exception;

    @Select("SELECT COUNT(*) FROM tm_session WHERE user_id = #{userId}")
    int getSizeByUser(@Nullable @Param("userId") String userId) throws Exception;

    @NotNull
    @Update("UPDATE tm_session SET date = #{date}, user_id = #{user_id}, role = #{role}")
    Session update(@NotNull Session session) throws Exception;

    @Delete("DELETE FROM tm_session;")
    void clear();
}
