package ru.t1.chubarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.model.Session;

import java.util.Collection;
import java.util.List;

public interface ISessionService {
    @NotNull
    Session add(@Nullable String userId, @Nullable Session model) throws Exception;

    @NotNull
    List<Session> findAll() throws Exception;

    @Nullable
    List<Session> findAll(@NotNull String userId) throws Exception;

    @NotNull
    Session findOneById(@NotNull String userId, @Nullable String id) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull
    Session remove(@NotNull String userId, @Nullable Session model) throws Exception;

    void removeAll(@NotNull String userId, @Nullable Collection<Session> models) throws Exception;

    @NotNull
    Session removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull
    Session removeOneById(@Nullable String id) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    int getSize() throws Exception;

}
