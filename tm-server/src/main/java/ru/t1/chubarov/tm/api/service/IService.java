package ru.t1.chubarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.IRepository;
import ru.t1.chubarov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    @NotNull
    M add(@Nullable M model) throws Exception;

    @NotNull
    Collection<M> add(@NotNull Collection<M> models) throws Exception;

    @NotNull
    Collection<M> set(@NotNull Collection<M> models) throws Exception;

    void clear() throws Exception;

    @NotNull
    List<M> findAll() throws Exception;

    @Nullable
    List<M> findAll(@Nullable Comparator<M> comparator) throws Exception;

    @NotNull
    M remove(@Nullable M model) throws Exception;

    @NotNull
    M findOneById(@NotNull String id) throws Exception;

    @NotNull
    M findOneByIndex(@NotNull Integer index) throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

}
