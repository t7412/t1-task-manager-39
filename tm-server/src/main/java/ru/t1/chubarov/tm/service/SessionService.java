package ru.t1.chubarov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.ISessionRepository;
import ru.t1.chubarov.tm.api.service.IConnectionService;
import ru.t1.chubarov.tm.api.service.ISessionService;
import ru.t1.chubarov.tm.exception.entity.ModelNotFoundException;
import ru.t1.chubarov.tm.exception.entity.SessionNotFoundException;
import ru.t1.chubarov.tm.exception.field.IdEmptyException;
import ru.t1.chubarov.tm.exception.field.UserIdEmptyException;
import ru.t1.chubarov.tm.model.Session;

import java.util.Collection;
import java.util.List;

public class SessionService implements ISessionService {

    @NotNull
    private final IConnectionService connectionService;

    public SessionService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    public Session add(@Nullable String userId, @Nullable Session model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.add(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return model;
    }

    @NotNull
    @Override
    public List<Session> findAll() throws Exception {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            return repository.findAll();
        }
    }

    @Nullable
    @Override
    public List<Session> findAll(@NotNull String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            return repository.findAllByUser(userId);
        }
    }

    @NotNull
    @Override
    public Session findOneById(@NotNull String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            @Nullable final Session model = repository.findOneById(userId, id);
            if (model == null) throw new SessionNotFoundException();
            return model;
        } catch (@NotNull final Exception e) {
            throw e;
        }
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        try ( @NotNull final SqlSession sqlSession = connectionService.getSqlSession()){
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            if (repository.existsById(userId, id) > 0) return true;
            else return false;
        }
    }

    @NotNull
    @Override
    public Session remove(@NotNull final String userId, @Nullable Session model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.removeOneById(userId, model.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return model;
    }

    @Override
    public void removeAll(@NotNull final String userId, @Nullable final Collection<Session> models) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            for (@NotNull final Session session : models) {
                repository.removeOneById(userId, session.getId());
            }
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Session removeOneById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Session model = new Session();
        model.setId(id);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.removeOneById(userId, model.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return model;
    }

    @NotNull
    @Override
    public Session removeOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Session model = new Session();
        model.setId(id);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.removeOneById(model.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return model;
    }

    @Override
    public int getSize(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            return repository.getSizeByUser(userId);
        }
    }

    @Override
    public int getSize() throws Exception {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            return repository.getSize();
        }
    }

}
