package ru.t1.chubarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.model.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    @NotNull
    Task create(@NotNull String userId, @NotNull String name, @NotNull String description) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    List<Task> findAll(@NotNull String userId) throws Exception;

    @NotNull
    List<Task> findAll() throws Exception;

    @Nullable
    List<Task> findAll(@NotNull String userId, @NotNull Comparator<Task> comparator) throws Exception;

    @NotNull
    Task findOneById(@NotNull String userId, @Nullable String id) throws Exception;

    @Nullable
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId) throws Exception;

    @NotNull
    Task add(@Nullable String userId, @Nullable Task model) throws Exception;

    @NotNull
    Task updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description) throws Exception;

    @NotNull
    Task changeTaskStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status) throws Exception;

    @NotNull
    Task remove(@NotNull String userId, @Nullable Task model) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    @NotNull
    Task removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    int getSize() throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    @NotNull
    Collection<Task> add(@NotNull Collection<Task> models) throws Exception;

    @NotNull
    Collection<Task> set(@NotNull Collection<Task> models) throws Exception;

    void clear();
}
