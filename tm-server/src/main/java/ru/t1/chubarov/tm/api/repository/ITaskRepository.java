package ru.t1.chubarov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
            , @Result(property = "project_id", column = "projectId")
    })
    List<Task> findAllByProjectId(@NotNull @Param("userId") String userId, @NotNull @Param("projectId") String projectId) throws Exception;

    @Insert("INSERT INTO tm_task (id, name, created, description, user_id, status, project_id)" +
            " VALUES (#{id}, #{name}, #{created}, #{description}, #{userId}, #{status}, #{projectId})")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
            , @Result(property = "project_id", column = "projectId")
    })
    void add(@NotNull final Task task);

    @Insert("INSERT INTO tm_task (id, name, created, description, user_id, status) VALUES (#{id}, #{name}, #{created}, #{description}, #{userId}, #{status})")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    void addByUser(@Nullable @Param("userId") String userId, @NotNull final Task task);

    @NotNull
    @Select("SELECT * FROM tm_task")
    List<Task> findAll() throws Exception;

    @NotNull
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId}")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    List<Task> findAllByUser(@Nullable @Param("userId") String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY #{name}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<Task> findAllByUserSort(@Nullable @Param("userId") String userId, @NotNull @Param("name") String name) throws Exception;

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} AND id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    Task findOneById(@Nullable @Param("userId") String userId, @Nullable @Param("id") String id) throws Exception;

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId} AND id = #{id}")
    void remove(@NotNull final Task model) throws Exception;

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId} AND id = #{id}")
    void removeOneById(@Nullable @Param("userId") String userId, @Nullable @Param("id") String id) throws Exception;

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId}")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    void removeAll(@Nullable @Param("userId") String userId) throws Exception;

    @Select("SELECT COUNT(*) FROM tm_task")
    int getSize() throws Exception;

    @Select("SELECT COUNT(*) FROM tm_task WHERE user_id = #{userId}")
    int getSizeByName(@Nullable @Param("userId") String userId) throws Exception;

    @NotNull
    @Update("UPDATE tm_task SET name = #{name}, created = #{created}, description = #{description}, user_id = #{userId}, status = #{status} WHERE id = #{id}")
    void update(@NotNull Task task) throws Exception;

    @Delete("DELETE FROM tm_task;")
    void clear();

    @Select("SELECT COUNT(*) FROM tm_task WHERE user_id = #{userId} AND id = #{id}")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    int existsById(@Nullable @Param("userId") String userId, @NotNull @Param("id") String id) throws Exception;

}
